Ext.application({
    name: 'petbase',

    views: [
        'AddPetFormView',
        'AddClientFormView',
        'PetBaseView',
        'PetGridView',
        'ClientGridView'
    ],

    controllers: [
        'PetBaseController'
    ],

    stores: [
        'PetStore',
        'ClientStore'
    ],

    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: {
                xtype: 'petBaseView'
            }
        });
    }
});