Ext.define('petbase.view.PetBaseView', {
    extend: 'Ext.panel.Panel',
    width: 500,
    height: 360,
    padding: 10,
    alias: 'widget.petBaseView',
    layout: 'border',
    items: [
        {
            xtype: 'petGridView',
            region: 'east'
        },
        {
            xtype: 'clientGridView',
            region: 'west',
            collapsible: true,
            collapsed: false
        }
    ],
    renderTo: Ext.getBody()
});