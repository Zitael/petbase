Ext.define('petbase.model.ClientModel', {
    extend: 'Ext.data.Model',
    fields: ['name', 'phone'],
    proxy: {
        type: 'rest',
        api: {
            create: 'client',
            read: 'client',
            destroy: 'client',
            update: 'client'
        },
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            writeRecordId: false
        }

    }
});