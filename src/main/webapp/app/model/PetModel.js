Ext.define('petbase.model.PetModel', {
    extend: 'Ext.data.Model',
    fields: ['name', 'kind', 'age'],
    proxy: {
        type: 'rest',
        api: {
            create: 'pet',
            read: 'pet',
            destroy: 'pet',
            update: 'pet'
        },
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            writeRecordId: false
        }

    }
});