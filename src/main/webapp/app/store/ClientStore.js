Ext.define('petbase.store.СlientStore', {
    extend: 'Ext.data.Store',
    requires : [
        'petbase.model.ClientModel'
    ],
    model: 'petbase.model.ClientModel',
    autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'rest',
        api: {
            create: 'client',
            read: 'client',
            destroy: 'client',
            update: 'client'
        },
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }

    }
});