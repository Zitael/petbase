Ext.define('petbase.store.PetStore', {
    extend: 'Ext.data.Store',
    requires : [
        'petbase.model.PetModel'
    ],
    model: 'petbase.model.PetModel',
    autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'rest',
        api: {
            create: 'pet',
            read: 'pet',
            destroy: 'pet',
            update: 'pet'
        },
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }

    }
});