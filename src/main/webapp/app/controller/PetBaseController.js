Ext.define('petbase.controller.PetBaseController', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            selector: 'petGridView',
            ref: 'petGridView'
        },
        {
            selector: 'petGridView button[action="add"]',
            ref: 'petGridAdd'
        },
        {
            selector: 'petGridView button[action="delete"]',
            ref: 'petGridDelete'
        },

        {
            selector: 'clientGridView',
            ref: 'clientGridView'
        },
        {
            selector: 'clientGridView button[action="add"]',
            ref: 'clientGridAdd'
        },
        {
            selector: 'clientGridView button[action="delete"]',
            ref: 'clientGridDelete'
        },

        {
            selector: 'addPetFormView',
            ref: 'addPetFormView'
        },

        {
            selector: 'addClientFormView',
            ref: 'addClientFormView'
        },

        {
            selector: 'petBaseView',
            ref: 'petBaseView'
        },

        {
            selector: 'addPetFormView textfield[name=name]',
            ref: 'addPetFormName'
        },
        {
            selector: 'addPetFormView textfield[name=age]',
            ref: 'addPetFormAge'
        },
        {
            selector: 'addPetFormView textfield[name=kind]',
            ref: 'addPetFormKind'
        },
        {
            selector: 'addPetFormView button[action=save]',
            ref: 'addPetFormSave'
        },

        {
            selector: 'addClientFormView textfield[name=name]',
            ref: 'addClientFormName'
        },
        {
            selector: 'addClientFormView textfield[name=phone]',
            ref: 'addClientFormPhone'
        },
        {
            selector: 'addClientFormView button[action=save]',
            ref: 'addClientFormSave'
        }
    ],

    init: function () {
        this.control({
            'petGridView  button[action=add]': {
                click: this.onAddPet
            },
            'petGridView  button[action=delete]': {
                click: this.onDelPet
            },
            'petGridView': {
                cellclick: this.onPetLineGrid
            },
            'addPetFormView  button[action=save]': {
                click: this.onSavePet
            },
            'addPetFormView  textfield[name=name]': {
                change: this.onPetValidation
            },
            'addPetFormView  textfield[name=age]': {
                change: this.onPetValidation
            },
            'addPetFormView  textfield[name=kind]': {
                change: this.onPetValidation
            },
            'clientGridView  button[action=add]': {
                click: this.onAddClient
            },
            'clientGridView  button[action=delete]': {
                click: this.onDelClient
            },
            'clientGridView': {
                cellclick: this.onClientLineGrid
            },
            'addClientFormView  button[action=save]': {
                click: this.onSaveClient
            },
            'addClientFormView  textfield[name=name]': {
                change: this.onClientValidation
            },
            'addClientFormView  textfield[name=phone]': {
                change: this.onClientValidation
            }
        });
    },

    onSavePet: function (button) {
        var me = this;
        var petModel = Ext.create('petbase.model.PetModel');
        petModel.set(this.getAddPetFormView().down('form').getValues());
        petModel.save({
            success: function (operation, response) {
                var objAjax = operation.data;
                Ext.getStore('PetStore').add(objAjax);
                me.getAddPetFormView().close();
            },
            failure: function (dummy, result) {
                Ext.MessageBox.show({
                    title: 'Дубликат!',
                    msg: 'Такой питомец уже существует',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }

        });
    },
    onSaveClient: function (button) {
        var me = this;
        var clientModel = Ext.create('petbase.model.ClientModel');
        clientModel.set(this.getAddClientFormView().down('form').getValues());
        clientModel.save({
            success: function (operation, response) {
                var objAjax = operation.data;
                Ext.getStore('ClientStore').add(objAjax);
                me.getAddClientFormView().close();
            },
            failure: function (dummy, result) {
                Ext.MessageBox.show({
                    title: 'Дубликат!',
                    msg: 'Такой клиент уже существует',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.ERROR
                });
            }

        });
    },

    onAddPet: function () {
        Ext.widget('addPetFormView');
    },
    onAddClient: function () {
        Ext.widget('addClientFormView');
    },


    onDelPet: function () {
        var sm = this.getPetGridView().getSelectionModel();
        var rs = sm.getSelection();
        this.getPetGridView().store.remove(rs[0]);
        this.getPetGridView().store.commitChanges();
        this.getPetGridDelete().disable();
    },

    onDelClient: function () {
        var sm = this.getClientGridView().getSelectionModel();
        var rs = sm.getSelection();
        this.getClientGridView().store.remove(rs[0]);
        this.getClientGridView().store.commitChanges();
        this.getClientGridDelete().disable();
    },

    // onChangeText: function () {
    //     Ext.getStore('PetStore').load({
    //         params: {
    //             search: this.getPetBaseView().down('searchPetView').getValues()
    //         }
    //     });
    // },

    onPetLineGrid: function () {
        this.getPetGridDelete().enable();
    },

    onClientLineGrid: function () {
        this.getClientGridDelete().enable();
    },

    onPetValidation: function () {
        if (this.getAddPetFormName().validate()
            && this.getAddPetFormAge().validate()
            && this.getAddPetFormKind().validate()) {
            this.getAddPetFormSave().enable();
        } else {
            this.getAddPetFormSave().disable();
        }
    },

    onClientValidation: function () {
        if (this.getAddClientFormName().validate()
            && this.getAddClientFormPhone().validate()) {
            this.getAddClientFormSave().enable();
        } else {
            this.getAddClientFormSave().disable();
        }
    }
});