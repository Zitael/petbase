package ru.alexandrov.petbase.entity;

import lombok.Data;

@Data
public class Client {
    private Long id;

    private String name;

    private Integer petId;

    private Integer phone;
}
