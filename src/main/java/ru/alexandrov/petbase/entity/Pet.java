package ru.alexandrov.petbase.entity;

import lombok.Data;
import java.io.Serializable;

@Data
public class Pet implements Serializable {
    private Long id;

    private String name;

    private String kind;

    private Integer age;

    private Integer ownerId;
}
