package ru.alexandrov.petbase;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.alexandrov.petbase.entity.Client;
import ru.alexandrov.petbase.entity.Pet;

@MappedTypes({Client.class, Pet.class})
@MapperScan("ru.alexandrov.petbase.mapper")
@SpringBootApplication
public class PetBaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(PetBaseApplication.class, args);
    }
}
