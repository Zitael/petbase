package ru.alexandrov.petbase.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import ru.alexandrov.petbase.entity.Pet;

import java.util.List;

@Mapper
public interface PetMapper {

    Pet findById(Long id);

    void add(Pet pet);

    void update(Pet pet);

    void delete(Pet pet);

    Pet findByPet(String name, Integer age);

    List<Pet> getPets();
}
