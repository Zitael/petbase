package ru.alexandrov.petbase.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.context.annotation.Bean;
import ru.alexandrov.petbase.entity.Client;

import java.util.List;

@Mapper
public interface ClientMapper {

    Client findById(Integer id);

    void add(Client client);

    void update(Client client);

    void delete(Client client);

    List<Client> getClients();

    Client findByClient(String name, Integer phone);

}
