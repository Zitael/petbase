package ru.alexandrov.petbase.controller;

import org.springframework.web.bind.annotation.*;
import ru.alexandrov.petbase.entity.Client;
import ru.alexandrov.petbase.mapper.ClientMapper;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
    private ClientMapper clientMapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public ClientController(ClientMapper clientMapper) {
        this.clientMapper = clientMapper;
    }

    @GetMapping
    public List<Client> getAll() {
        return clientMapper.getClients();
    }

    @PostMapping
    public void add(@RequestBody Client client){
        clientMapper.add(client);
    }

    @DeleteMapping(value = "{id}")
    public String deletePet(@RequestBody Client client, @PathVariable("id") String id) {
        clientMapper.delete(client);
        return "delete";
    }

    @PutMapping(value = "{id}")
    public String updatePet(@RequestBody Client client, @PathVariable("id") String id) {
        clientMapper.update(client);
        return "update";
    }
}
