package ru.alexandrov.petbase.controller;

import org.springframework.web.bind.annotation.*;
import ru.alexandrov.petbase.entity.Pet;
import ru.alexandrov.petbase.mapper.PetMapper;

import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {
    private PetMapper petMapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public PetController(PetMapper petMapper) {
        this.petMapper = petMapper;
    }

    @GetMapping
    public List<Pet> getAll() {
        return petMapper.getPets();
    }

    @PostMapping
    public void add(@RequestBody Pet pet) {
        petMapper.add(pet);
    }

    @DeleteMapping(value = "{id}")
    public String deletePet(@PathVariable("id") Long id, @RequestBody Pet pet) {
        petMapper.delete(pet);
        return "delete";
    }

    @PutMapping(value = "{id}")
    public String updatePet(@PathVariable("id") Long id, @RequestBody Pet pet) {
        petMapper.update(pet);
        return "update";
    }
}
